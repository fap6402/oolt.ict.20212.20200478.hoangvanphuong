package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;
import java.util.ArrayList;
import java.util.Scanner;

public class Aims {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Order> orders = new ArrayList<>();

        while (true) {
            showMenu();
            System.out.print("> ");
            int option = Integer.valueOf(scanner.nextLine());
            
            int itemId = -1;
            int orderId = -1;
            Order yourOrder = null;
            switch (option) {
                case 0:
                    System.out.println("Exiting...");
                    return;
                case 1:
                    if (Order.underLimit()) {
                        yourOrder = new Order();
                        orders.add(yourOrder);
                        System.out.println("Creating order with id: " + yourOrder.getId());
                    } else {
                        System.out.println("Order limit reached!");
                    }
                    break;
                case 2:
                    orderId = askOrderId(scanner);
                    yourOrder = orders.get(orderId -1);
                    yourOrder.addMedia(askItemBasicInfo(scanner));
                    break;
                case 3:
                    orderId = askOrderId(scanner);
                    yourOrder = orders.get(orderId -1);

                    itemId = askItemId(scanner);
                    if (yourOrder.hasItemId(itemId)) {
                        yourOrder.removeMedia(itemId);
                    }
                    break;
                case 4:
                    orderId = askOrderId(scanner);
                    orders.get(orderId - 1).printOrder();
                    break;
                default:
                    System.out.println("Try again!");
                    break;
            }

            System.out.println();
        }
    }

    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static int askOrderId(Scanner scanner) {
        System.out.print("Enter order id: ");
        return Integer.valueOf(scanner.nextLine());
    }

    public static int askItemId(Scanner scanner) {
        System.out.print("Enter item id: ");
        return Integer.valueOf(scanner.nextLine());
    }

    public static Media askItemBasicInfo(Scanner scanner) {
        System.out.print("Enter title: ");
        String title = scanner.nextLine();
        System.out.print("Enter category: ");
        String category = scanner.nextLine();
        System.out.print("Enter cost: ");
        float cost = Float.valueOf(scanner.nextLine());
        return new Media(title, category, cost);
    }
}