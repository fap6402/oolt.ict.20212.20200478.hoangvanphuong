public class Aims {
    public static void main(String[] args) throws Exception {

        Order anOrder = new Order();

        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);

        anOrder.addDigitalVideoDisc(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addDigitalVideoDisc(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);
        anOrder.addDigitalVideoDisc(dvd3);
        System.out.print("Total Cost is: ");
        System.out.println(anOrder.totalCost());

        DigitalVideoDisc[] dvds = new DigitalVideoDisc[2];
        dvds[0] = new DigitalVideoDisc("A", "Film","F.A.P",100,20.00f);
        dvds[1] = new DigitalVideoDisc("B ", "Film","F.A.P.S",1000,2000.00f);
        dvds[2] = new DigitalVideoDisc("L1_2", "Test", "Test", 120, 20.00f);
        anOrder.addDigitalVideoDisc(dvds);
        System.out.print("Total Cost is: ");
        System.out.println(anOrder.totalCost());

        anOrder.printOrder();

        DigitalVideoDisc[] dvdList2 = new DigitalVideoDisc[7];
        for (int i = 0; i < dvdList2.length; i++) {
            dvdList2[i] = new DigitalVideoDisc("L2_" + i, "Test", "Test", 120, 20.00f);
        }
        anOrder.addDigitalVideoDisc(dvdList2[0], dvdList2[1]);  // qtyOrdered = 8
        anOrder.addDigitalVideoDisc(dvdList2[2], dvdList2[3]);  // qtyOrdered = 10
        anOrder.removeDigitalVideoDisc(dvdList2[3]);            // qtyOrdered = 9
        anOrder.addDigitalVideoDisc(dvdList2[4], dvdList2[5]);  // qtyOrdered = 10
        anOrder.addDigitalVideoDisc(dvdList2[6]);               // qtyOrdered = 10

        System.out.print("Total Cost is: ");
        System.out.println(anOrder.totalCost());
    }
}