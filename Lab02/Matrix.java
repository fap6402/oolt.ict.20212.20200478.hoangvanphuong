import java.util.Scanner;
import java.util.Arrays;
public class Matrix {
    public static void main(String[] args) {
        Scanner matrix = new Scanner(System.in);
        System.out.print("Enter the number of row: ");
        int row = matrix.nextInt();
        System.out.print("Enter the number of column: ");
        int column = matrix.nextInt();
        int[][] matrix1 = new int[row][column];
        for(int i = 0; i < row; i++)
            for(int j = 0; j < column; j++)
                matrix1[i][j] = matrix.nextInt();

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++)
                System.out.print(matrix1[i][j] + "\t");
            System.out.printf("\n");
        }
        System.out.printf("\n");

        int[][] matrix2 = new int[row][column];
        for(int i = 0; i < row; i++)
            for(int j = 0; j < column; j++)
                matrix2[i][j] = matrix.nextInt();

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++)
                System.out.print(matrix2[i][j] + "\t");
            System.out.printf("\n");
        }
        System.out.printf("\n");

        int[][] sum = new int[row][column]; 
        for(int i = 0; i < row; i++)
            for(int j = 0; j < column; j++)
                sum[i][j] = matrix1[i][j] + matrix2[i][j];    
                
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++)
                System.out.print(sum[i][j] + "\t");
            System.out.printf("\n");
            }
        System.out.printf("\n");
        System.out.println(Arrays.deepToString(sum));
    }
}