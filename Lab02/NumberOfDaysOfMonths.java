import java.util.Scanner;
public class NumberOfDaysOfMonths {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        System.out.println("What's the month?");
        String month = keyboard.nextLine();
        int check;
        if (month.matches("January|Jan.|Jan|1"))
            check = 1;
        else if (month.matches("February|Feb.|Feb|2"))
            check = 2;
        else if (month.matches("March|Mar.|Mar|3"))
            check = 3;
        else if (month.matches("April|Apr.|Apr|4"))
            check = 4;
        else if (month.matches("May|5"))
            check = 5;
        else if (month.matches("June|Jun|6"))
            check = 6;
        else if (month.matches("July|Jul|7"))
            check = 7;
        else if (month.matches("August|Aug.|Aug|8"))
            check = 8;
        else if (month.matches("September|Sep.|Sep|9"))
            check = 9;
        else if (month.matches("October|Oct.|Oct|10"))
            check = 10;
        else if (month.matches("November|Nov.|Nov|11"))
            check = 11;
        else if (month.matches("December|Dec.|Dec|12"))
            check = 12;
        else
            check = 0;
        if (check == 0){
            System.out.println("Invalid month. Please re-enter!");
            System.exit(0);
        }
        System.out.println("What's the year?");
        int year = keyboard.nextInt();
        if (year <= 0){
            System.out.println("Invalid year. Please re-enter!");
            System.exit(0);
        }
        switch(check){
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                System.out.println("In " + month + " of " + year + " we have "  + " 31 days.");
            case 4: case 6: case 9: case 11:
                System.out.println("In " + month + " of " + year + " we have "  + " 30 days.");
            case 2:{
                if (year % 4 == 0){
                    if (year % 100 == 0 && year % 400 != 0)
                        System.out.println("In " + month + " of " + year + " we have "  + " 28 days.");
                    else 
                        System.out.println("In " + month + " of " + year + " we have "  + " 29 days.");      
                }   
                else
                    System.out.println("In " + month + " of " + year + " we have "  + " 28 days.");
            }
        }
    }  
}
