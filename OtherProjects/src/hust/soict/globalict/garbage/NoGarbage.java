package hust.soict.globalict.garbage;

import java.util.Scanner;
import java.net.URL;
import java.nio.file.Paths;

public class NoGarbage {
    public static void main(String[] args) {
        // Access data.txt in the same folder as NoGarbage.java
        URL path = NoGarbage.class.getResource("data.txt");

        try (Scanner scanner = new Scanner(Paths.get(path.toURI()))) {
            long start = System.currentTimeMillis();

            String s = "";
            StringBuilder sb = new StringBuilder();
            while (scanner.hasNextLine()) {
                sb.append(scanner.nextLine());
            }
            s = sb.toString();

            System.out.println(System.currentTimeMillis() - start);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
