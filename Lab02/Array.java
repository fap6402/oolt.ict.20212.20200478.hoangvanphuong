import java.util.Scanner;
public class Array {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        int n;
        do {
            System.out.print("Enter the number of elements: ");
            n = keyboard.nextInt();
        } while (n < 0);
        int array[] = new int[n];
        for (int i = 0; i < n; i++) 
            array[i] = keyboard.nextInt();
        for (int i = 0; i < n; i++)
            System.out.print(array[i] + "\t");
        System.out.printf("\n");
        for(int i = 0; i < n - 1; i++)
            for(int j = i + 1; j < n; j++){
                if( array[i] > array[i+1]){
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;
                }
            }
        for (int i = 0; i < n; i++)
            System.out.print(array[i] + "\t");
        System.out.printf("\n");
        int sum = 0;
        for (int i = 0; i < n; i++)
            sum += array[i];
        System.out.println(sum);
        float average = (float)sum/n;
        System.out.println(average);
    }
}