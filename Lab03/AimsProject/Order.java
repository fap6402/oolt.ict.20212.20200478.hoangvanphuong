import java.util.ArrayList;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;

    private ArrayList<DigitalVideoDisc> itemsOrdered;

    public Order() {
        this.itemsOrdered = new ArrayList<>();
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (itemsOrdered.size() + 1 <= MAX_NUMBER_ORDERED) {
            if (itemsOrdered.contains(disc)) {
                System.out.println("The disc has been added!");
            } else {
                itemsOrdered.add(disc);
            }
        }

        if (itemsOrdered.size() == MAX_NUMBER_ORDERED) {
                System.out.println("The order is full!");
        }
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        if (itemsOrdered.contains(disc)) {
            itemsOrdered.remove(disc);
        }
    }

    public float totalCost() {
        float sum = 0;
        for (DigitalVideoDisc item : itemsOrdered) {
            sum += item.getCost();
        }

        return sum;
    }
}