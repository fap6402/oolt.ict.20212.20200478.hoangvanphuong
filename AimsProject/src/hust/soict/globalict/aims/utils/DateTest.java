package hust.soict.globalict.aims.utils;

import hust.soict.globalict.test.utils.MyDate;
import java.util.Scanner;

public class DateTest {
    public static void main(String[] args) {
        MyDate[] dates = new MyDate[3];

        dates[0] = new MyDate("27th", "August", "2002");
        System.out.print("Format: ");
        Scanner sc = new Scanner(System.in);
        String pattern = sc.nextLine();
        dates[0].print(pattern);

        dates[1] = new MyDate();
        dates[2] = new MyDate(22, 2, 2022);
        System.out.print("Compare dates[1] and dates[2]: ");
        System.out.println(DateUtils.compareDate(dates[1], dates[2]));

        System.out.println("Before:");
        for (MyDate date : dates) {
            date.print();
        }

        DateUtils.sortDate(dates);
        System.out.println("After:");
        for (MyDate date : dates) {
            date.print();
        }
    }
}
