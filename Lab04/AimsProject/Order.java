import java.util.ArrayList;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERED = 5;
    public static int nbOrders = 0;
    private MyDate dateOrdered;

    private ArrayList<DigitalVideoDisc> itemsOrdered;

    public Order() {
        if (nbOrders + 1 <= MAX_LIMITED_ORDERED) {
            itemsOrdered = new ArrayList<>();
            dateOrdered = new MyDate();
            nbOrders++;
        } else {
            System.out.println("Order limit reached");
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (itemsOrdered.size() + 1 <= MAX_NUMBER_ORDERED) {
            if (itemsOrdered.contains(disc)) {
                System.out.println("The disc has been added!");
            } 
            else {
                itemsOrdered.add(disc);
            }
        }

        if (itemsOrdered.size() == MAX_NUMBER_ORDERED) {
                System.out.println("The order is full!");
        }
    }

    /* public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList){
        for(int i = 0; i <= dvdList.length; i++){
            if (itemsOrdered.size() + 1 <= MAX_NUMBER_ORDERED) {
                if (itemsOrdered.contains(dvdList)) {
                    System.out.println("The dvd has been added!");
                } 
                else {
                    itemsOrdered.add(dvdList[i]);
                }
            }
    
            if (itemsOrdered.size() == MAX_NUMBER_ORDERED) {
                    System.out.println("The order is full!");
            }
        }
    } */

    public void addDigitalVideoDisc(DigitalVideoDisc... dvdList){
        if (itemsOrdered.size() + 1 <= MAX_NUMBER_ORDERED) {
            for(DigitalVideoDisc disc : dvdList){
                this.addDigitalVideoDisc(disc);
            }
        }
        else {
            System.out.println("Oversize!");
        }
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        if (itemsOrdered.contains(disc)) {
            itemsOrdered.remove(disc);
        }
    }

    public float totalCost() {
        float sum = 0;
        for (DigitalVideoDisc item : itemsOrdered)
            sum += item.getCost();
        return sum;
    }

    public void printOrder() {
        System.out.print("Date: ");
        dateOrdered.print();
        System.out.println("Ordered Items:");

        for (int i = 0; i < itemsOrdered.size(); i++) {
            DigitalVideoDisc dvd = itemsOrdered.get(i);
            System.out.print((i + 1) + ". DVD - ");
            System.out.print(dvd.getTitle() + " - ");
            System.out.print(dvd.getCategory() + " - ");
            System.out.print(dvd.getDirector() + " - ");
            System.out.print(dvd.getLength() + ": ");
            System.out.print(dvd.getCost() + " $");
            System.out.print("\n");
        }
        System.out.println("Total cost: " + this.totalCost());
    }
}
