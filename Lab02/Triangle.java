import java.util.Scanner;
public class Triangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of lines: ");
        int height = scanner.nextInt();
        for(int i=1;i <= height;i++){
            for(int j = 1; j <= height-i; j++)
                System.out.printf(" ");
            for(int j = 1; j <= 2*i-1; j++){
                System.out.printf("*");
            }
            System.out.printf("\n");
            // hang 1: 4 space
            // hang 2: 3 space
            // hang 3: 2 space
            // hang i: n - i space
            // int i = 1 -> n - i
        }
    }
}