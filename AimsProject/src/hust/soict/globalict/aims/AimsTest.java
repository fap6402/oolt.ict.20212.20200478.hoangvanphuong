package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class AimsTest {
    public static void main(String[] args) {
        Order anOrder = new Order();

        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        anOrder.addMedia(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addMedia(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);
        anOrder.addMedia(dvd3);

        System.out.println();
        anOrder.printOrder();

        DigitalVideoDisc[] dvdList1 = new DigitalVideoDisc[3];
        dvdList1[0] = new DigitalVideoDisc("L1_0", "Test", "Test", 120, 20.00f);
        dvdList1[1] = new DigitalVideoDisc("L1_1", "Test", "Test", 120, 20.00f);
        dvdList1[2] = new DigitalVideoDisc("L1_2", "Test", "Test", 120, 20.00f);
        anOrder.addMedia(dvdList1);
        // anOrder.addMedia(dvdList[0], dvdList[1], dvdList[2]);

        System.out.println();
        anOrder.printOrder();

        DigitalVideoDisc[] dvdList2 = new DigitalVideoDisc[7];
        for (int i = 0; i < dvdList2.length; i++) {
            dvdList2[i] = new DigitalVideoDisc("L2_" + i, "Test", "Test", 120, 20.00f);
        }
        anOrder.addMedia(dvdList2[0], dvdList2[1]);  // qtyOrdered = 8
        anOrder.addMedia(dvdList2[2], dvdList2[3]);  // qtyOrdered = 10
        anOrder.removeMedia(dvdList2[3]);            // qtyOrdered = 9
        anOrder.addMedia(dvdList2[4], dvdList2[5]);  // qtyOrdered = 10
        anOrder.addMedia(dvdList2[6]);               // qtyOrdered = 10

        System.out.println();
        anOrder.printOrder();
    }
}