package hust.soict.globalict.garbage;

import java.util.Scanner;
import java.net.URL;
import java.nio.file.Paths;

public class GarbageCreator {
    public static void main(String[] args) {
        // Access data.txt in the same folder as GarbageCreator.java
        URL path = GarbageCreator.class.getResource("data.txt");

        try (Scanner scanner = new Scanner(Paths.get(path.toURI()))) {
            long start = System.currentTimeMillis();

            String s = "";
            while (scanner.hasNextLine()) {
                s += scanner.nextLine();
            }

            System.out.println(System.currentTimeMillis() - start);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
