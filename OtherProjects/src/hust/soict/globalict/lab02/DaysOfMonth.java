package hust.soict.globalict.lab02;

import java.util.Scanner;

public class DaysOfMonth {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        int month;
        while (true) {
            System.out.print("Enter month: ");
            String inputMonth = reader.nextLine();

            if (inputMonth.matches("^January|Jan.|Jan|1$"))
                month = 1;
            else if (inputMonth.matches("^February|Feb.|Feb|2$"))
                month = 2;
            else if (inputMonth.matches("^March|Mar.|Mar|3$"))
                month = 3;
            else if (inputMonth.matches("^April|Apr.|Apr|4$"))
                month = 4;
            else if (inputMonth.matches("^May|5$"))
                month = 5;
            else if (inputMonth.matches("^June|Jun|6$"))
                month = 6;
            else if (inputMonth.matches("^July|Jul|7$"))
                month = 7;
            else if (inputMonth.matches("^August|Aug.|Aug|8$"))
                month = 8;
            else if (inputMonth.matches("^September|Sep.|Sep|9$"))
                month = 9;
            else if (inputMonth.matches("^October|Oct.|Oct|10$"))
                month = 10;
            else if (inputMonth.matches("^November|Nov.|Nov|11$"))
                month = 11;
            else if (inputMonth.matches("^December|Dec.|Dec|12$"))
                month = 12;
            else
                continue;

            break;
        }

        int year;
        while (true) {
            System.out.print("Enter year: ");
            String inputYear = reader.nextLine();

            try {
                year = Integer.valueOf(inputYear);
            } catch (Exception e) {
                continue;
            }

            break;
        }

        boolean isLeapYear;
        if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
            isLeapYear = true;
        } else {
            isLeapYear = false;
        }

        int days;
        switch (month) {
            case 2:
                days = isLeapYear ? 29 : 28;
                break;

            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                days = 31;
                break;

            default:
                days = 30;
                break;
        }

        System.out.print("The input month has " + days + " days.");
    }
}
