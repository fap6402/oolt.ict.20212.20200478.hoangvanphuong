package hust.soict.globalict.aims.utils;

import hust.soict.globalict.test.utils.MyDate;        
import java.time.LocalDate;
import java.util.Arrays;

public class DateUtils {
    public static int compareDate(MyDate date1, MyDate date2) {
        LocalDate dateTime1 = date1.getLocalDate();
        LocalDate dateTime2 = date2.getLocalDate();
        return dateTime1.compareTo(dateTime2);
    }

    public static void sortDate(MyDate dates[]) {
        Arrays.sort(dates, DateUtils::compareDate);
    }
}
